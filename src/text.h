#ifndef TEXT_H
#define TEXT_H

#include <string>
#include "gameobject.h"

/*!
 * \brief The Text class
 *GameObject for text showing
 */
class Text : public GameObject
{
    std::string text;
public:
    Text();
    std::string getText() const;
    void setText(const std::string &value);

    bool hitTest(double x, double y)
    {
        return false;
    }
};

#endif // TEXT_H
