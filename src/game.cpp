#include "game.h"

#include <cmath>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <string>
#include <sstream>
#include <stdexcept>

#include "render/circlerenderer.h"
#include "render/textrenderer.h"
#include "circle.h"

using namespace std;


void Game::resizeImpl(int width, int height)
{
    m_width = width;
    m_height = height;

    glMatrixMode (GL_MODELVIEW);
    glViewport (0, 0, m_width, m_height);
    glLoadIdentity();
}

bool isGameObjectForDelete(GameObject* go)
{
    return go == 0;
}

Game::Game(int width, int height)
{
    if (!glfwInit())
        throw runtime_error("glfwInit error");

    if (glfwOpenWindow(width, height, 8, 8, 8, 8, 8, 0, GLFW_WINDOW) != GL_TRUE)
    {
        glfwTerminate();
        throw runtime_error("glfwOpenWindow error");
    }
    glfwGetWindowSize(&m_width, &m_height);
    glfwSetWindowTitle("Circles");

    m_score = 0;
    m_scoreGameObject = new Text();
    m_scoreGameObject->setColor(1,1,1);

    m_scoreGameObject->setPosition(0, 0);
    m_scoreGameObject->setRender(new TextRenderer());
    m_scoreGameObject->setText("0");

    m_gameObjects.push_back(m_scoreGameObject);

    srand(time(0));
}


Game::~Game()
{
    delete m_scoreGameObject;
    glfwTerminate();
}

void Game::mouseCallbackImpl(int button, int action)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
    {
        int x,y;
        glfwGetMousePos(&x, &y);

        for (vector<GameObject*>::iterator it = m_gameObjects.begin(); it != m_gameObjects.end(); ++it)
        {
            GameObject* go = *it;
            if (!go)
                continue;

            if (go->hitTest(x, m_height - y))
            {
                Bounty* bounty = dynamic_cast<Bounty*>(go);
                if (bounty)
                {
                    m_score += bounty->bounty();
                    ostringstream ss;
                    ss << m_score;

                    m_scoreGameObject->setText(ss.str());
                }

                delete go;
                *it = 0;
                break;
            }
        }
    }
}

void Game::gameLoop()
{
    glfwSetWindowSizeCallback(resize);
    glfwSetMouseButtonCallback(mouseCallback);

    double startTime = glfwGetTime();
    double oldTime = startTime;
    do
    {
        double currentTime = glfwGetTime();

        update(currentTime - startTime, currentTime - oldTime);
        render();

        oldTime = currentTime;
    } while (glfwGetWindowParam(GLFW_OPENED) && glfwGetKey(GLFW_KEY_ESC) != GLFW_PRESS);

}

void Game::update(double gameTime, double elapsed)
{
    static double lastspawn = 0;

    if (lastspawn + m_spawnDelay < gameTime)
    {
        Circle* c = new Circle();
        //random radius beetwen m_minr and m_maxr
        double radius = (double) rand() / RAND_MAX * (m_maxr - m_minr) + m_minr;

        c->setRender(new CircleRenderer());
        c->setRadius(radius);
        //random x coord within window
        c->setX((double) rand() / RAND_MAX * (m_width - 2 * radius)  + radius);
        c->setY(m_height + radius);

        //less radius = more speed
        c->setSpeed(0, -m_impulse / radius);
        //random color
        c->setColorRed((double) rand() / RAND_MAX);
        c->setColorGreen((double) rand() / RAND_MAX);
        c->setColorBlue((double) rand() / RAND_MAX);

        //linear bounty for popping base of radius, min = 0, max = 100
        c->setBounty((m_maxr - radius) / (m_maxr - m_minr) * 100);

        m_gameObjects.push_back(c);

        lastspawn = gameTime;
    }

    for (vector<GameObject*>::iterator it = m_gameObjects.begin(); it != m_gameObjects.end(); ++it)
    {
        GameObject* go = *it;
        if (!go)
            continue;
        go->update(elapsed);

        //TODO add bounds for GameObject to avoid dynamic_cast
        Circle* circle = dynamic_cast<Circle*> (go);
        if (circle)
        {
            if (circle->y() + circle->radius() < 0)
            {
                delete circle;
                *it = 0;
            }
        } else if (go->y() < 0)
        {
            //remove object and mark for delete with nullptr
            delete go;
            *it = 0;
        }
    }
    //remove all nullptr from array
    m_gameObjects.erase(remove_if(m_gameObjects.begin(), m_gameObjects.end(), isGameObjectForDelete), m_gameObjects.end());
}

void Game::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, m_width, 0, m_height);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glColor3d(1,1,1);

    for (vector<GameObject*>::iterator it = m_gameObjects.begin(); it != m_gameObjects.end(); ++it)
    {
        IRenderComponent* render = (*it)->render();
        if (render)
            (*it)->render()->render(*it);
    }

    glfwSwapBuffers();
    glfwPollEvents();
}
