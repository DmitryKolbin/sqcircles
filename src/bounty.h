#ifndef BOUNTY_H
#define BOUNTY_H
/*!
 * \brief The Bounty class
 * Simple class for bounty counting
 */
class Bounty
{
    int m_bounty;
public:
    virtual ~Bounty(){}

    int bounty() const;
    void setBounty(int bounty);
};

#endif // BOUNTY_H
