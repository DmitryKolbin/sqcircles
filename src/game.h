#ifndef GAME_H
#define GAME_H

#include <GL/gl.h>
#include <GL/glfw.h>
#include <vector>
#include "gameobject.h"
#include "text.h"

using namespace std;

/*!
 * \brief The Game class
 *Main game logic and visualization class
 */
class Game
{
    int m_width;
    int m_height;
    double m_spawnDelay;
    double m_minr;
    double m_maxr;
    double m_impulse;
    int m_score;

    Text* m_scoreGameObject;

    vector<GameObject*> m_gameObjects;
public:

    static Game& getInstance()
    {
        static Game instance;
        return instance;
    }

    ~Game();
    /*!
     * \brief gameLoop
     * main game loop
     */
    void gameLoop();

    /*!
     * \brief spawnDelay
     * \return primitives spawn delay
     */
    double spawnDelay() const
    {
        return m_spawnDelay;
    }
    /*!
     * \brief set primitves spawn delay
     * \param spawnDelay
     */
    void setSpawnDelay(double spawnDelay)
    {
        m_spawnDelay = spawnDelay;
    }
    /*!
     * \brief minr
     * \return minimum radius of circles
     */
    double minr() const
    {
        return m_minr;
    }
    /*!
     * \brief set minimum radius of circles
     * \param minr
     */
    void setMinr(double minr)
    {
        m_minr = minr;
    }
    /*!
     * \brief maxr
     * \return maximum radius of circles
     */
    double maxr() const
    {
        return m_maxr;
    }
    /*!
     * \brief set maximum radius of circles
     * \param maxr
     */
    void setMaxr(double maxr)
    {
        m_maxr = maxr;
    }
    /*!
     * \brief impulse of circles
     * \return
     */
    double impulse() const
    {
        return m_impulse;
    }
    /*!
     * \brief set impulse of circles
     * \param impulse
     */
    void setImpulse(double impulse)
    {
        m_impulse = impulse;
    }

private:
    Game(int width=0, int height=0);
    Game(const Game&);
    Game& operator=(const Game&);

    /*!
     * \brief update game state
     * \param gameTime time since game start
     * \param elapsed time since last update
     */
    void update(double gameTime, double elapsed);
    /*!
     * \brief render game objects
     */
    void render();
    /*!
     * \brief mouseCallback function for glfwSetMouseButtonCallback()
     * \sa glfwSetMouseButtonCallback()
     */
    static void mouseCallback(int button, int action)
    {
        getInstance().mouseCallbackImpl(button, action);
    }
    /*!
     *\sa mouseCallback()
     */
    void mouseCallbackImpl(int button, int action);

    /*!
     * \brief resize function for glfwSetWindowSizeCallback()
     * \param width
     * \param height
     * \sa glfwSetWindowSizeCallback()
     */
    static void resize(int width, int height)
    {
        getInstance().resizeImpl(width, height);
    }
    /*!
     * \sa resize()
     */
    void resizeImpl(int width, int height);

};

#endif // GAME_H
