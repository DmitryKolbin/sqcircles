#ifndef CIRCLE_H
#define CIRCLE_H
#include "gameobject.h"
#include "bounty.h"
/*!
 * \brief The Circle class
 *
 *This class implements circle primitive.
 *
 */
class Circle : public GameObject, public Bounty
{
    /*!
     * \brief radius of circle
     */
    double m_radius;
    /*!
     * \brief bounty for click
     */
    double m_bounty;
public:
    Circle();
    /*!
     * \brief hitTest
     * \param x
     * \param y
     * \return
     * \sa GameObject::hitTest()
     */
    virtual bool hitTest(double x, double y);
    /*!
     * \brief radius
     * \return radius of circle
     */
    double radius() const;
    /*!
     * \brief set radius of circle
     * \param radius
     */
    void setRadius(double radius);
};

#endif // CIRCLE_H
