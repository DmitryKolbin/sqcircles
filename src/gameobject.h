#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "render/irender.h"
/*!
 * \brief The GameObject class
 *Base class for all rendering objects
 */
class GameObject
{
    /*!
     * \brief m_x
     *x coordinate of object
     */
    double m_x;
    /*!
     * \brief m_y
     *y coordinate of object
     */
    double m_y;
    /*!
     * \brief m_speedx
     *x speed of object
     */
    double m_speedx;
    /*!
     * \brief m_speedx
     *y speed of object
     */
    double m_speedy;

    /*!
     * \brief m_colorRed
     *red color part of object
     */
    double m_colorRed;
    /*!
     * \brief m_colorGreen
     *green color part of object
     */
    double m_colorGreen;
    /*!
     * \brief m_colorBlue
     *blue color part of object
     */
    double m_colorBlue;
    /*!
     * \brief m_render
     *Component for rendering
     *
     *0 if not existed
     */
    IRenderComponent *m_render;
public:
    GameObject() {
        m_render = 0;
    }
    virtual ~GameObject() {
        if (m_render)
            delete m_render;
    }
    /*!
     * \brief x
     * \return x position of GamObject
     */
    double x() const
    {
        return m_x;
    }
    /*!
     * \brief set x postion of GameObject
     * \param x
     */
    void setX(double x)
    {
        m_x = x;
    }
    /*!
     * \brief y
     * \return y posiotion of GameObject
     */
    double y() const
    {
        return m_y;
    }
    /*!
     * \brief set y postion of GameObject
     * \param y
     */
    void setY(double y)
    {
        m_y = y;
    }
    /*!
     * \brief set position of GameObject
     * \param cx
     * \param cy
     */
    void setPosition(double x, double y)
    {
        m_x = x;
        m_y = y;
    }
    /*!
     * \brief speedx
     * \return speed along x-axis
     */
    double speedx() const
    {
        return m_speedx;
    }
    /*!
     * \brief set speed along x-axis
     * \param speedx
     */
    void setSpeedx(double speedx)
    {
        m_speedx = speedx;
    }
    /*!
     * \brief speedy
     * \return speed along y-axis
     */
    double speedy() const
    {
        return m_speedy;
    }
    /*!
     * \brief set speed along y-axis
     * \param speedy
     */
    void setSpeedy(double speedy)
    {
        m_speedy = speedy;
    }
    /*!
     * \brief set speed of GameObject
     * \param speedx
     * \param speedy
     */
    void setSpeed(double speedx, double speedy)
    {
        m_speedx = speedx;
        m_speedy = speedy;
    }
    /*!
     * \brief render
     * \return render component for GameObject
     *0 if not existed
     */
    IRenderComponent *render() const
    {
        return m_render;
    }
    /*!
     * \brief set render component for GameObject
     *  0 if not existed
     * \param render
     */
    void setRender(IRenderComponent *render)
    {
        m_render = render;
    }
    /*!
     * \brief colorBlue
     * \return blue part of color
     */
    double colorBlue() const
    {
        return m_colorBlue;
    }

    /*!
     * \brief set blue part of color for GameObject
     * \param colorBlue
     */
    void setColorBlue(double colorBlue)
    {
        m_colorBlue = colorBlue;
    }
    /*!
     * \brief colorGreen
     * \return green part of color
     */
    double colorGreen() const
    {
        return m_colorGreen;
    }
    /*!
     * \brief set green part of color for GameObject
     * \param colorGreen
     */
    void setColorGreen(double colorGreen)
    {
        m_colorGreen = colorGreen;
    }
    /*!
     * \brief colorRed
     * \return return red part of color
     */
    double colorRed() const
    {
        return m_colorRed;
    }
    /*!
     * \brief set red part of color for GameObject
     * \param colorRed
     */
    void setColorRed(double colorRed)
    {
        m_colorRed = colorRed;
    }
    /*!
     * \brief set rgb color for GameObject
     * \param red part of color
     * \param green part of color
     * \param blue part of color
     * \sa setColorRed(), setColorGreen(), setColorBlue()
     */
    void setColor(double red, double green, double blue)
    {
        m_colorRed = red;
        m_colorGreen = green;
        m_colorBlue = blue;
    }
    /*!
     * \brief hitTest
     * \param x coordinate of point
     * \param y coordinate of point
     * \return true if point contained in GameObject
     */
    virtual bool hitTest(double x, double y) = 0;
    /*!
     * \brief update object internal state
     *
     * \param elapsed time since last update is seconds
     */
    virtual void update(double elapsed) {
        m_x += m_speedx * elapsed;
        m_y += m_speedy * elapsed;
    }
};


#endif // GAMEOBJECT_H
