#include "game.h"
#include <iostream>

int main()
{
    try
    {
    Game &game = Game::getInstance();

    game.setImpulse(10000);
    game.setMinr(10);
    game.setMaxr(100);
    game.setSpawnDelay(0.5);


    game.gameLoop();
    }
    catch(const std::exception& ex)
    {
        std::cerr<< ex.what() << std::endl;
    }

    return 0;
}

