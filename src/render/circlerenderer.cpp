#include "circlerenderer.h"
#include "../circle.h"
#include <cmath>

using namespace std;

CircleRenderer::CircleRenderer(size_t pointsCount)
    : m_pointsCount(pointsCount)
{

}

CircleRenderer::~CircleRenderer()
{

}


void CircleRenderer::render(GameObject* go)
{
    Circle* circle = dynamic_cast<Circle*> (go);
    if (!circle)
        return;

    glBegin(GL_TRIANGLE_FAN);
    glColor3d(circle->colorRed(), circle->colorGreen(), circle->colorBlue());
    for (int i = 0; i < m_pointsCount; ++i)
    {
        float x = circle->radius() * cos(i * M_PI * 2 / m_pointsCount);
        float y = circle->radius() * sin(i * M_PI * 2 / m_pointsCount);

        glVertex2f(circle->x() + x, circle->y() + y);
    }
    glEnd();
}
