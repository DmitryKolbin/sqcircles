#ifndef TEXTRENDERER_H
#define TEXTRENDERER_H

#include <GL/gl.h>
#include <FTGL/ftgl.h>

#include "irender.h"
#include "../text.h"
/*!
 * \brief The TextRenderer class
 *This class implements text rendering routine
 */
class TextRenderer : public IRenderComponent
{
    FTPixmapFont m_font;
public:
    TextRenderer();

    virtual void render(GameObject* go);
};

#endif // TEXTRENDERER_H
