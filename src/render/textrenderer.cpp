#include "textrenderer.h"
#include <stdexcept>

TextRenderer::TextRenderer() : m_font("arial.ttf")
{
    if (m_font.Error())
        throw std::runtime_error("Error loading arail font");
    m_font.FaceSize(72);
}

void TextRenderer::render(GameObject *go)
{
    Text* text = dynamic_cast<Text*>(go);
    if (text)
    {
        glRasterPos2f(text->x(), text->y());
        glColor3d(text->colorRed(), text->colorGreen(), text->colorBlue());
        m_font.Render(text->getText().c_str());
    }
}
