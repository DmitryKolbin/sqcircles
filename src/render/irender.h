#ifndef IRENDER_H
#define IRENDER_H

class GameObject;
/*!
 * \brief The IRenderComponent interface
 *Interface for rendering routine
 */
class IRenderComponent
{
public:
    /*!
     * \brief render following GameObject
     * \param go
     */
    virtual void render(GameObject* go) =0;
    virtual ~IRenderComponent() { }
};

#endif // IRENDERCOMPONENT_H
