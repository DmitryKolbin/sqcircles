#ifndef CIRCLERENDERER_H
#define CIRCLERENDERER_H

#include "irender.h"
#include "../gameobject.h"
#include <vector>
#include <utility>
#include <GL/gl.h>

/*!
 * \brief The CircleRenderer class
 *This class implements circle rendering routine
 */

class CircleRenderer : public IRenderComponent
{
    int m_pointsCount;

    CircleRenderer(const CircleRenderer& );
    CircleRenderer& operator=(const CircleRenderer& );
public:
    /*!
     * \brief CircleRenderer
     * \param pointsCount number of points for circle approximation
     */
    CircleRenderer(std::size_t pointsCount = 40);

    ~CircleRenderer();

    void render(GameObject *go);
};

#endif // CIRCLERENDERER_H
