#include "circle.h"
#include <cmath>

Circle::Circle()
{
}

bool Circle::hitTest(double px, double py)
{
    return sqrt((x() - px)*(x() - px) + (y() - py)* (y() - py)) < m_radius;
}

double Circle::radius() const
{
    return m_radius;
}

void Circle::setRadius(double radius)
{
    m_radius = radius;
}
